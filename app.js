"use strict";
process.on(`uncaughtException`, console.error);
// Heroku
if ( process.env.PORT == null ) process.env.PORT = 5000;

const TelegramBot = require('node-telegram-bot-api'),
	fs = require('fs'),
	server = require('express')();

/*
 *  EDIT CONFIG VARS HERE
 */
const bot = {
	token : 'GET THIS FROM @BotFather',
	webhook : true,
	urlWebhook : 'https://EXAMPLE.herokuapp.com/', // only needed for webhook
	settings: {
		maxGiveValue: 10,
	},

	// DON'T EDIT BELOW
	bot : undefined,
	info : undefined,
	on : (cmd, cb) => bot.bot.onText( new RegExp(`^\/${cmd}(?:@${bot.info.username} | |$)(.*)`, 'i'), cb ),
	reply : (req, msg) => bot.bot.sendMessage( req.chat.id, msg, {reply_to_message_id: req.message_id} ),
};
bot.bot = new TelegramBot( bot.token, {polling: !bot.webhook} );
bot.bot.setWebHook( (bot.webhook) ? bot.urlWebhook + bot.token : '' );

const data = {
	path : 'data.json',
	v : undefined,
	add: ({chat}, name, val) => {
		if (! data.v[chat.id] )
			data.v[chat.id] = {};

		if (! data.v[chat.id][name] )
			data.v[chat.id][name] = +val;
		else
			data.v[chat.id][name] += +val;

		fs.writeFile( data.path, JSON.stringify(data.v), ()=>{});

		return data.v[chat.id][name];
	},
};
data.v = JSON.parse( fs.readFileSync(data.path) );

const commands = {
	/*
	 *  ADD CUSTOM COMMANDS BELOW
	 */

	/*
	echo : (msg, [, text]) => {
		bot.reply( msg, text );
	},
	*/

	addValue : (msg, [, text], opts = {}) => {
		try {
			const kv = text.split(' '),
				name = kv[0],
				gain = kv[1];

			if ( kv.length < 2 )
				throw 'Missing one or more arguments';

			if (! gain.match(/^\d+$/) )
				throw 'Invalid value, not a number';

			if ( gain > bot.settings.maxGiveValue && !opts.fromAddTopValue )
				throw `Invalid value, too high (max allowed ${bot.settings.maxGiveValue})`;

			const val = data.add( msg, name, +gain );

			bot.reply( msg, `• ${name}: ${val} (+${gain})` );
		} catch (e) {
			bot.reply( msg, `• ERROR: ${e}!` );
			console.error(e);
		}
	},

	addTopValue : (msg, [, text]) => {
		try {
			const name = text.split(' ')[0];

			if ( name.length < 1 )
				throw 'Specify a name';

			commands.addValue( msg, [null, `${name} ${bot.settings.maxGiveValue + 1}`], {fromAddTopValue:true} );
		} catch (e) {
			bot.reply( msg, `• ERROR: ${e}!` );
			console.error(e);
		}
	},

	count : msg => {
		const txt = [],
			v = [];

		for ( let key in data.v[msg.chat.id] )
			v.push( {
				name : key,
				n : data.v[key],
			} );

		v.sort( (a, b) => b.n - a.n ).forEach( e => {
			txt.push( `• ${e.name}: ${e.n}` );
		});

		bot.reply( msg, txt.join('\n') );
	},

	reload : msg => {
		fs.readFile( data.path, (err, file) => {
			try {
				if (err)
					throw err;

				data.v = JSON.parse( file );

				bot.reply( msg, '• RELOAD DONE' );
			} catch (e) {
				bot.reply( msg, `• RELOAD ERROR: ${e}` );
				console.error(e);
			}
		});
	},


	/*
	 *  ADD CUSTOM COMMANDS ABOVE
	 */
};

bot.bot.getMe().then( me => {
	bot.info = me;

	for ( let key in commands )
		bot.on( key, commands[key] );

	server.get( '/data.json', (req, res) => {
		res.end( JSON.stringify( data.v, null, '\t' ) );
	});
	if ( bot.webhook ) {
	    server.use( require('body-parser').json() );
		server.post( '/' + bot.token, (req, res) => {
			bot.bot.processUpdate( req.body );
			res.sendStatus( 200 );
		});
	}
	server.listen( process.env.PORT, '0.0.0.0' );
});
